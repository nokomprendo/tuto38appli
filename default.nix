{ pkgs ? import <nixpkgs> {} }:

let 

  tuto38lib-src = fetchTarball {
    url = "https://gitlab.com/nokomprendo/tuto38lib/-/archive/v1.0/tuto38lib-v1.0.tar.gz";
    sha256 = "1di8ms0g1j9kih9qg1s42p9wi5xxbm7h3n9as6fbxqfbfa75w9nf";
  };

  tuto38lib = pkgs.callPackage tuto38lib-src {};

in pkgs.stdenv.mkDerivation {
  name = "tuto38appli";
  src = ./.;
  buildInputs = with pkgs; [ cmake tuto38lib ];
}

